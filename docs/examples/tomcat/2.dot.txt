digraph "G" {
    graph ["rankdir"="LR",];
    "detect";
    "msg.jdk12";
    "msg.jsse";
    "msg.puretls";
    "msg.commons-dbcp";
    "init";
    "init" -> "detect";
    "init" -> "msg.jdk12";
    "init" -> "msg.jsse";
    "init" -> "msg.puretls";
    "init" -> "msg.commons-dbcp";
    "prepare.jaxp101";
    "prepare.jaxp101" -> "detect";
    "include.jaxp";
    "include.jaxp" -> "detect";
    "prepare.jaxp11";
    "prepare.jaxp11" -> "detect";
    "prepare.jaxp11" -> "include.jaxp";
    "prepare.xerces";
    "prepare.xerces" -> "detect";
    "prepare.jaxp";
    "prepare.jaxp" -> "prepare.jaxp101";
    "prepare.jaxp" -> "prepare.jaxp11";
    "prepare.jaxp" -> "prepare.xerces";
    "prepare";
    "prepare" -> "init";
    "prepare" -> "prepare.dirs";
    "prepare" -> "prepare.jaxp";
    "prepare.dirs";
    "prepare.dirs" -> "init";
    "tomcat_util";
    "tomcat_util" -> "init";
    "tomcat.jar";
    "tomcat.jar" -> "init";
    "tomcat.jar" -> "tomcat_modules";
    "stop-tomcat.jar";
    "stop-tomcat.jar" -> "init";
    "tomcat_core";
    "tomcat_core" -> "init";
    "tomcat-startup";
    "tomcat-startup" -> "init";
    "facade22";
    "facade22" -> "init";
    "commons-prepare";
    "commons-prepare" -> "prepare";
    "tomcat_modules";
    "tomcat_modules" -> "init";
    "tomcat_modules" -> "commons-prepare";
    "jasper";
    "jasper" -> "init";
    "tomcat-jars";
    "tomcat-jars" -> "prepare";
    "tomcat-jars" -> "tomcat_util";
    "tomcat-jars" -> "stop-tomcat.jar";
    "tomcat-jars" -> "tomcat_core";
    "tomcat-jars" -> "jasper";
    "tomcat-jars" -> "tomcat_modules";
    "tomcat-jars" -> "tomcat.jar";
    "tomcat-jars" -> "facade22";
    "tomcat-jars" -> "tomcat-startup";
    "webapps";
    "webapps" -> "prepare";
    "dist";
    "dist" -> "dist.prepare";
    "dist" -> "javadoc";
    "dist" -> "dist.war";
    "dist.prepare";
    "dist.prepare" -> "main";
    "dist.prepare" -> "webapps";
    "dist.prepare" -> "tomcat-jars";
    "javadoc";
    "javadoc" -> "main";
    "javadoc" -> "webapps";
    "javadoc" -> "tomcat-jars";
    "dist.war";
    "dist.war" -> "dist.prepare";
    "main";
    "main" -> "tomcat";
    "main" -> "webapps";
    "tomcat";
    "tomcat" -> "prepare";
    "tomcat" -> "tomcat-jars";
    subgraph "cluster:1" {
        "label"=" src/j2ee/build.xml";
    }
    subgraph "cluster:2" {
        "label"=" src/tests/build.xml";
    }
    subgraph "cluster:3" {
        "label"=" proposals/PasswordPrompter/build.xml";
    }
    subgraph "cluster:4" {
        "label"=" proposals/StreamHandler/build.xml";
    }
}
