package net.sourceforge.vizant;

/**
 * writer.
 * 
 * @author <a href="mailto:kengo@tt.rim.or.jp">KOSEKI Kengo</a>
 */
public interface VizWriter {
    public void print(String str);
    public void println(String str);
}
